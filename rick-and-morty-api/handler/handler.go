package handler

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"rick-and-morty/rick-and-morty-api/structs"

	"github.com/labstack/echo"
)

func AllCharacters(c echo.Context) error {
	res, err := http.Get("https://rickandmortyapi.com/api/character/")
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]string{
			"message": err.Error(),
		})
	}

	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]string{
			"message": err.Error(),
		})
	}

	var characterRes character.Response
	json.Unmarshal(data, &characterRes)

	return c.JSON(http.StatusOK, characterRes.Result)
}
