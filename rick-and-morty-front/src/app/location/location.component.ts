import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '../models/location';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit {
  location: Location;
  title: string = "LOCATION"

  constructor(
    private route: ActivatedRoute,
    private router: Router
    
  ) { }

  ngOnInit() {
    this.route
      .queryParams
      .subscribe(params => {
        this.location = JSON.parse(params['location'])
      });
  }

  backToMain() {
    this.router.navigate(['/main']);
  }

}
