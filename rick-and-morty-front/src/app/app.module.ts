import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { CharacterService } from './services/character.service';
import { HttpClientModule } from '@angular/common/http';
import { MainComponent } from './main/main.component';
import { CharacterComponent } from './character/character.component';
import { LocationComponent } from './location/location.component';

const routes: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: MainComponent },
  { path: 'location', component: LocationComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    CharacterComponent,
    LocationComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule
  ],
  providers: [
    CharacterService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
