import { Component, OnInit, Input } from '@angular/core';
import { Character } from '../models/character';
import { Router } from '@angular/router';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.scss']
})
export class CharacterComponent implements OnInit {
  @Input("Character") character: Character;
  
  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  goToLocation() {
      this.router.navigate(['/location'], { queryParams: { location: JSON.stringify(this.character.location) } });
  }

}
