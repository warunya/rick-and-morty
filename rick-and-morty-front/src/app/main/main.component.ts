import { Component, OnInit } from '@angular/core';
import { Character } from '../models/character';
import { CharacterService } from '../services/character.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  characterList: Character[];
  title: string = "THE RICK AND MORTY"

  constructor(
    public characterService: CharacterService
  ) {
    this.characterService.findAll().subscribe((res: Character[]) => {
      this.characterList = res;
      console.log(this.characterList)
    });
  }

  ngOnInit() {
  }

}
