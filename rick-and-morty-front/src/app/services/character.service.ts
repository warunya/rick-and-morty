import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Character } from 'src/app/models/character';

@Injectable()
export class CharacterService {
    constructor(
        private http: HttpClient
    ) { }

    findAll(): Observable<Character[]> {
        const url = "/api/characters";
        // const url = "http://localhost:1323/characters";
        // return this.http.get(url);
        return this.http.get<any[]>(url);
    }
}
